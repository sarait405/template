<!-- ======= Header ======= -->
<header id="header" class="fixed-top" >
    <div class="container d-flex">

        <div class="logo mr-auto float-left">
            <h1 class="text-light "><a href="index.html">Maxim</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav class="nav-menu d-none d-lg-block float-right">
            <ul>
                <li><a href="#contact">تماس با ما</a></li>
                <li class="drop-down"><a href="">پنل کاربری</a>
                    <ul>
                        @auth
                            @if(auth::user()->role == 2)
                                <li><a href="{{route('profile' , Auth::user()->id)}}">پروفایل کاربری</a></li>
                            @endif
                            @if(auth::user()->role == 1)
                                <li><a href="{{route('admin')}}" target="_blank">پنل مدیریت</a></li>
                            @endif
                            <li>
                                <form action="{{route('logout')}}" method="post">
                                    @csrf
                                    <input type="submit" class="btn btn-success" style="margin-right: 10px;text-align:center;width: 60px;" value="خروج">
                                </form>
                            </li>
                        @else
                            <li><a href="{{route('register')}}">ثبت نام</a></li>
                            <li><a href="{{route('login')}}">ورود</a></li>
                        @endauth
                    </ul>
                </li>
                <li><a href="#team">اعضای تیم</a></li>
                <li><a href="#portfolio">نمونه کارها </a></li>
                <li><a href="#services">خدمات</a></li>
                <li><a href="#about">درباره ما</a></li>
                <li class="active"><a href="index.html">صفحه اصلی</a></li>

            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
